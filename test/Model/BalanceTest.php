<?php
/**
 * BalanceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Service managing iZettle merchants inventory
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * BalanceTest Class Doc Comment
 *
 * @category    Class
 * @description Contains balance for a specific variant
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class BalanceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Balance"
     */
    public function testBalance()
    {
    }

    /**
     * Test attribute "product_uuid"
     */
    public function testPropertyProductUuid()
    {
    }

    /**
     * Test attribute "variant_uuid"
     */
    public function testPropertyVariantUuid()
    {
    }

    /**
     * Test attribute "balance"
     */
    public function testPropertyBalance()
    {
    }
}
