# Inventory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**variants** | [**\Swagger\Client\Model\VariantChangeHistory[]**](VariantChangeHistory.md) | Collection of changes to balances of all variants that is currently has a stock value | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


