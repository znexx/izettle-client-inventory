# NotificationSettingsExternal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_receivers** | **string[]** |  | [optional] 
**notify_below_balance** | **float** |  | 
**interval** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


