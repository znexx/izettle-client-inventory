# StartTrackingRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_uuid** | **string** | Id of the product you want to start tracking | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


