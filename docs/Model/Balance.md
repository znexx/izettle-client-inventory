# Balance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_uuid** | **string** | Identifies the product | 
**variant_uuid** | **string** | Identifies the variant | 
**balance** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


