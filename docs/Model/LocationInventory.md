# LocationInventory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_uuid** | **string** |  | [optional] 
**tracked_products** | **string[]** | Collection of UUIDs of all products that is currently tracked for this location | [optional] 
**variants** | [**\Swagger\Client\Model\LocationBalance[]**](LocationBalance.md) | Collection of balances of all variants that is currently has a stock value | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


