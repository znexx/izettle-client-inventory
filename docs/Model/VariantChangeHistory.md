# VariantChangeHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_uuid** | **string** |  | [optional] 
**variant_uuid** | **string** |  | [optional] 
**change** | **string** |  | [optional] 
**changed** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


