# OrganizationSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**etag** | **string** |  | [optional] 
**default_refund_to_location** | **string** |  | [optional] 
**notification_settings** | [**\Swagger\Client\Model\NotificationSettingsExternal**](NotificationSettingsExternal.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


