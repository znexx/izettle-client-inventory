# ProductChangeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_uuid** | **string** | Product identifier as UUID1 | 
**tracking_status_change** | **string** | Status of the change | 
**variant_changes** | [**\Swagger\Client\Model\VariantChangeDTO[]**](VariantChangeDTO.md) | List of balance changes. Optional | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


