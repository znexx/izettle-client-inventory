# BulkUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_changes** | [**\Swagger\Client\Model\ProductChangeDTO[]**](ProductChangeDTO.md) |  | 
**return_balance_for_location_uuid** | **string** | Id of the location the balances should be fetched for | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


