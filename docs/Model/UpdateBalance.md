# UpdateBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changes** | [**\Swagger\Client\Model\ProductVariantChange[]**](ProductVariantChange.md) | List of changes. | 
**return_balance_for_location_uuid** | **string** | Location uuid whose balances will be returned. Optional (defaults to STORE | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


