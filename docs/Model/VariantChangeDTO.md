# VariantChangeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**variant_uuid** | **string** | Id of the variant you want change | 
**from_location_uuid** | **string** | Id of location you want to move inventory from (source) | 
**to_location_uuid** | **string** | Id of location you want to move inventory to (destination) | 
**change** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


