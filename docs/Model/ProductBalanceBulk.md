# ProductBalanceBulk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_uuid** | **string** | Identifies the location | 
**variants** | [**\Swagger\Client\Model\Balance[]**](Balance.md) | List of balances | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


