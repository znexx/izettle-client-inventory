# GetBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_type** | **string** | The location type for which to fetch the balance | 
**product_uuids** | **string[]** | The products for which to fetch the balance | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


