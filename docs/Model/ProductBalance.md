# ProductBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_uuid** | **string** | Identifies the location | 
**variants** | [**\Swagger\Client\Model\LocationBalance[]**](LocationBalance.md) | List of balances. Might be empty | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


