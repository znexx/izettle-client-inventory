# Swagger\Client\LocationsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLocation**](LocationsApi.md#addLocation) | **POST** /organizations/{organizationUuid}/locations/add | 
[**createLocations**](LocationsApi.md#createLocations) | **POST** /organizations/{organizationUuid}/locations | 
[**getAllLocations**](LocationsApi.md#getAllLocations) | **GET** /organizations/{organizationUuid}/locations | 
[**getLocationsTemplate**](LocationsApi.md#getLocationsTemplate) | **GET** /organizations/{organizationUuid}/locations/template | 
[**updateLocation**](LocationsApi.md#updateLocation) | **PUT** /organizations/{organizationUuid}/locations/{locationUuid} | 


# **addLocation**
> addLocation($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\CreateLocationRequest(); // \Swagger\Client\Model\CreateLocationRequest | 

try {
    $apiInstance->addLocation($organization_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->addLocation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\CreateLocationRequest**](../Model/CreateLocationRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createLocations**
> createLocations($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\CreateLocationsRequest(); // \Swagger\Client\Model\CreateLocationsRequest | 

try {
    $apiInstance->createLocations($organization_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->createLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\CreateLocationsRequest**](../Model/CreateLocationsRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllLocations**
> \Swagger\Client\Model\Location[] getAllLocations($organization_uuid, $generate)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$generate = true; // bool | 

try {
    $result = $apiInstance->getAllLocations($organization_uuid, $generate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->getAllLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **generate** | **bool**|  | [optional] [default to true]

### Return type

[**\Swagger\Client\Model\Location[]**](../Model/Location.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLocationsTemplate**
> \Swagger\Client\Model\CreateLocationsRequest getLocationsTemplate($organization_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getLocationsTemplate($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->getLocationsTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\CreateLocationsRequest**](../Model/CreateLocationsRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateLocation**
> updateLocation($organization_uuid, $location_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$location_uuid = "location_uuid_example"; // string | 
$body = new \Swagger\Client\Model\UpdateLocationRequest(); // \Swagger\Client\Model\UpdateLocationRequest | 

try {
    $apiInstance->updateLocation($organization_uuid, $location_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling LocationsApi->updateLocation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **location_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\UpdateLocationRequest**](../Model/UpdateLocationRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

