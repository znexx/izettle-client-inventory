# Swagger\Client\InventoryApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulkChange**](InventoryApi.md#bulkChange) | **POST** /organizations/{organizationUuid}/inventory/bulk | 
[**bulkUpdate**](InventoryApi.md#bulkUpdate) | **POST** /organizations/{organizationUuid}/v2/inventory/bulk | 
[**getInventoryByLocationType**](InventoryApi.md#getInventoryByLocationType) | **GET** /organizations/{organizationUuid}/inventory/locations | 
[**getInventoryForLocation**](InventoryApi.md#getInventoryForLocation) | **GET** /organizations/{organizationUuid}/inventory/locations/{locationUuid} | 
[**getInventoryForLocationAndProducts**](InventoryApi.md#getInventoryForLocationAndProducts) | **POST** /organizations/{organizationUuid}/inventory/products | 
[**getInventoryForProduct**](InventoryApi.md#getInventoryForProduct) | **GET** /organizations/{organizationUuid}/inventory/locations/{locationUuid}/products/{productUuid} | 
[**inactivateTrackingForProduct**](InventoryApi.md#inactivateTrackingForProduct) | **DELETE** /organizations/{organizationUuid}/inventory/products/{productUuid} | 
[**startTrackingProduct**](InventoryApi.md#startTrackingProduct) | **POST** /organizations/{organizationUuid}/inventory | 
[**updateBalanceForProduct**](InventoryApi.md#updateBalanceForProduct) | **PUT** /organizations/{organizationUuid}/inventory | 


# **bulkChange**
> \Swagger\Client\Model\ProductBalanceBulk bulkChange($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\BulkUpdateRequest(); // \Swagger\Client\Model\BulkUpdateRequest | 

try {
    $result = $apiInstance->bulkChange($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->bulkChange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\BulkUpdateRequest**](../Model/BulkUpdateRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductBalanceBulk**](../Model/ProductBalanceBulk.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bulkUpdate**
> \Swagger\Client\Model\ProductBalanceBulk bulkUpdate($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\BulkUpdateRequest(); // \Swagger\Client\Model\BulkUpdateRequest | 

try {
    $result = $apiInstance->bulkUpdate($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->bulkUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\BulkUpdateRequest**](../Model/BulkUpdateRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductBalanceBulk**](../Model/ProductBalanceBulk.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInventoryByLocationType**
> \Swagger\Client\Model\LocationInventory getInventoryByLocationType($organization_uuid, $type)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$type = "type_example"; // string | 

try {
    $result = $apiInstance->getInventoryByLocationType($organization_uuid, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->getInventoryByLocationType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **type** | **string**|  |

### Return type

[**\Swagger\Client\Model\LocationInventory**](../Model/LocationInventory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInventoryForLocation**
> \Swagger\Client\Model\LocationInventory getInventoryForLocation($organization_uuid, $location_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$location_uuid = "location_uuid_example"; // string | Location Identifier as UUID1

try {
    $result = $apiInstance->getInventoryForLocation($organization_uuid, $location_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->getInventoryForLocation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **location_uuid** | [**string**](../Model/.md)| Location Identifier as UUID1 |

### Return type

[**\Swagger\Client\Model\LocationInventory**](../Model/LocationInventory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInventoryForLocationAndProducts**
> \Swagger\Client\Model\LocationInventory getInventoryForLocationAndProducts($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization identifier as an UUID
$body = new \Swagger\Client\Model\GetBalance(); // \Swagger\Client\Model\GetBalance | 

try {
    $result = $apiInstance->getInventoryForLocationAndProducts($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->getInventoryForLocationAndProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization identifier as an UUID |
 **body** | [**\Swagger\Client\Model\GetBalance**](../Model/GetBalance.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\LocationInventory**](../Model/LocationInventory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInventoryForProduct**
> \Swagger\Client\Model\ProductBalance getInventoryForProduct($organization_uuid, $location_uuid, $product_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$location_uuid = "location_uuid_example"; // string | Location Identifier as UUID
$product_uuid = "product_uuid_example"; // string | Product Identifier as UUID

try {
    $result = $apiInstance->getInventoryForProduct($organization_uuid, $location_uuid, $product_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->getInventoryForProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **location_uuid** | [**string**](../Model/.md)| Location Identifier as UUID |
 **product_uuid** | [**string**](../Model/.md)| Product Identifier as UUID |

### Return type

[**\Swagger\Client\Model\ProductBalance**](../Model/ProductBalance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inactivateTrackingForProduct**
> inactivateTrackingForProduct($organization_uuid, $product_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$product_uuid = "product_uuid_example"; // string | 

try {
    $apiInstance->inactivateTrackingForProduct($organization_uuid, $product_uuid);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inactivateTrackingForProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **product_uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **startTrackingProduct**
> \Swagger\Client\Model\ProductBalance startTrackingProduct($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\StartTrackingRequest(); // \Swagger\Client\Model\StartTrackingRequest | 

try {
    $result = $apiInstance->startTrackingProduct($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->startTrackingProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\StartTrackingRequest**](../Model/StartTrackingRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductBalance**](../Model/ProductBalance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBalanceForProduct**
> \Swagger\Client\Model\ProductBalance updateBalanceForProduct($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\InventoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\UpdateBalance(); // \Swagger\Client\Model\UpdateBalance | 

try {
    $result = $apiInstance->updateBalanceForProduct($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->updateBalanceForProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\UpdateBalance**](../Model/UpdateBalance.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductBalance**](../Model/ProductBalance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

