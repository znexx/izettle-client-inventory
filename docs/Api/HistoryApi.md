# Swagger\Client\HistoryApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getVariantBalanceHistory**](HistoryApi.md#getVariantBalanceHistory) | **GET** /organizations/{organizationUuid}/history/locations/{locationUuid} | 


# **getVariantBalanceHistory**
> \Swagger\Client\Model\Inventory getVariantBalanceHistory($organization_uuid, $location_uuid, $balance_change_type)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\HistoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$location_uuid = "location_uuid_example"; // string | Location Identifier as UUID1
$balance_change_type = "balance_change_type_example"; // string | 

try {
    $result = $apiInstance->getVariantBalanceHistory($organization_uuid, $location_uuid, $balance_change_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoryApi->getVariantBalanceHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **location_uuid** | [**string**](../Model/.md)| Location Identifier as UUID1 |
 **balance_change_type** | **string**|  |

### Return type

[**\Swagger\Client\Model\Inventory**](../Model/Inventory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

