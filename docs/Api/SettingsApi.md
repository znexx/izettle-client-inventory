# Swagger\Client\SettingsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSettings**](SettingsApi.md#getSettings) | **GET** /organizations/{organizationUuid}/settings | Retrieves settings for the organization. Returns 404 if organization does not have settings.
[**storeSettings**](SettingsApi.md#storeSettings) | **POST** /organizations/{organizationUuid}/settings | Store settings for the organization.
[**updateSettings**](SettingsApi.md#updateSettings) | **PUT** /organizations/{organizationUuid}/settings | 


# **getSettings**
> \Swagger\Client\Model\OrganizationSettings getSettings($organization_uuid)

Retrieves settings for the organization. Returns 404 if organization does not have settings.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getSettings($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->getSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\OrganizationSettings**](../Model/OrganizationSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storeSettings**
> \Swagger\Client\Model\OrganizationSettings storeSettings($organization_uuid, $body)

Store settings for the organization.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\OrganizationSettingsRequest(); // \Swagger\Client\Model\OrganizationSettingsRequest | 

try {
    $result = $apiInstance->storeSettings($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->storeSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\OrganizationSettingsRequest**](../Model/OrganizationSettingsRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\OrganizationSettings**](../Model/OrganizationSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSettings**
> updateSettings($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\OrganizationSettingsRequest(); // \Swagger\Client\Model\OrganizationSettingsRequest | 

try {
    $apiInstance->updateSettings($organization_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->updateSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\OrganizationSettingsRequest**](../Model/OrganizationSettingsRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

